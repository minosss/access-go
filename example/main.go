package main

import (
	"log"

	access "github.com/minosss/access-go"
)

// User .
type User struct {
	permissions []string
}

// Allowed ...
func (u *User) Allowed(action string) bool {
	actions := []string{}
	publicPerms := acs.GetPublicPermissions()
	for _, p := range publicPerms {
		actions = append(actions, p.Actions()...)
	}
	actions = append(actions, acs.GetPermissionAllowedActions(u.permissions...)...)
	for _, a := range actions {
		if a == action {
			return true
		}
	}
	return false
}

var acs access.Access

// main ...
func main() {

	// define all permissions
	// hash: controller -> actions
	// map->action: controller/action
	acs = access.New()
	acs.Map("public", access.Hash{
		"users": "login",
	}, access.Options{Public: true})
	acs.Map("view_user", access.Hash{
		"users": []string{"index", "show"},
	}, access.Options{})
	acs.Map("edit_user", access.Hash{
		"users": "update",
	}, access.Options{})

	user := &User{
		permissions: []string{"view_user"},
	}

	log.Printf("user allowed view action? %v", user.Allowed("users/index"))
	log.Printf("user allowed update action? %v", user.Allowed("users/update"))
	log.Printf("user allowed (public)login action? %v", user.Allowed("users/login"))
}
