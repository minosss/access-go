package access

import (
	"errors"
	"fmt"
)

var (
	errNotSupport       = errors.New("Actions type not support")
	errPermissionExisit = errors.New("Permission name already exists")
)

type (
	// Access ...
	Access interface {
		Map(name string, hash Hash, options Options) error
		AddPermission(p Permission) error
		GetPermission(name string) (Permission, bool)
		GetPermissionAllowedActions(names ...string) []string
		GetPermissions() []Permission
		GetPublicPermissions() []Permission
	}

	// Permission ...
	Permission interface {
		Name() string
		Actions() []string
		IsPublic() bool
	}

	// Hash ...
	Hash map[string]interface{}

	// Options ...
	Options struct {
		Public bool
	}
)

// New ...
func New() Access {
	return &access{map[string]Permission{}}
}

type permission struct {
	name    string
	actions []string
	public  bool
}

// Name ...
func (p *permission) Name() string {
	return p.name
}

// Actions ...
func (p *permission) Actions() []string {
	return p.actions
}

// IsPublic ...
func (p *permission) IsPublic() bool {
	return p.public
}

type access struct {
	permissions map[string]Permission
}

// Map ...
func (a *access) Map(name string, hash Hash, options Options) error {
	perm := permission{name, []string{}, options.Public}
	for ctrl, actions := range hash {
		actType := fmt.Sprintf("%T", actions)
		if actType == "string" {
			perm.actions = append(perm.actions, fmt.Sprintf("%s/%s", ctrl, actions))
		} else if actType == "[]string" {
			actionsArr := actions.([]string)
			for _, v := range actionsArr {
				perm.actions = append(perm.actions, fmt.Sprintf("%s/%s", ctrl, v))
			}
		} else {
			return errNotSupport
		}
	}
	err := a.AddPermission(&perm)
	return err
}

// AddPermission ...
func (a *access) AddPermission(p Permission) error {
	if a.permissions[p.Name()] != nil {
		return errPermissionExisit
	}
	a.permissions[p.Name()] = p
	return nil
}

// GetPermission ...
func (a *access) GetPermission(name string) (Permission, bool) {
	perm := a.permissions[name]
	if perm != nil {
		return perm, true
	}
	return &permission{}, false
}

// GetPermissionAllowedActions ...
func (a *access) GetPermissionAllowedActions(names ...string) (actions []string) {
	for _, n := range names {
		if perm, ok := a.GetPermission(n); ok {
			actions = append(actions, perm.Actions()...)
		}
	}
	return
}

// GetPermissions ...
func (a *access) GetPermissions() (permissions []Permission) {
	for _, p := range a.permissions {
		permissions = append(permissions, p)
	}
	return
}

// GetPublicPermissions ...
func (a *access) GetPublicPermissions() (permissions []Permission) {
	for _, p := range a.permissions {
		if p.IsPublic() {
			permissions = append(permissions, p)
		}
	}
	return
}
