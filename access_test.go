package access_test

import (
	"testing"

	access "github.com/minosss/access-go"
	"github.com/stretchr/testify/assert"
)

func Test_New(t *testing.T) {
	t.Parallel()
	a := assert.New(t)

	acs := access.New()
	a.Empty(acs.GetPublicPermissions())
	a.Empty(acs.GetPermissions())
}

type myPermission struct {
	name    string
	actions []string
	public  bool
}

func (m *myPermission) Name() string {
	return m.name
}

func (m *myPermission) Actions() []string {
	return m.actions
}

func (m *myPermission) IsPublic() bool {
	return m.public
}

func Test_Map(t *testing.T) {
	t.Parallel()
	a := assert.New(t)

	acs := access.New()
	acs.Map("foo", access.Hash{}, access.Options{Public: true})
	acs.Map("bar", access.Hash{}, access.Options{})

	a.Equal(len(acs.GetPublicPermissions()), 1)
	a.Equal(len(acs.GetPermissions()), 2)
	a.NotNil(acs.GetPermission("foo"))
	a.NotNil(acs.GetPermission("bar"))
}

func Test_AddPermission(t *testing.T) {
	t.Parallel()
	a := assert.New(t)

	acs := access.New()
	foo := &myPermission{
		name:    "foo",
		actions: []string{"index", "show"},
		public:  false,
	}
	acs.AddPermission(foo)
	a.Empty(acs.GetPublicPermissions())

	f, _ := acs.GetPermission("foo")
	a.NotNil(f)
	a.Equal(f.Name(), "foo")
	a.Equal(f.IsPublic(), false)
	a.Equal(f.Actions(), []string{"index", "show"})
}

func Test_GetPermissionAllowedActions(t *testing.T) {
	t.Parallel()
	a := assert.New(t)

	acs := access.New()
	acs.Map("bar", access.Hash{
		"bar": "show",
	}, access.Options{})
	acs.Map("foo", access.Hash{
		"foo": []string{"index", "show"},
	}, access.Options{})

	a.Equal(acs.GetPermissionAllowedActions("bar"), []string{"bar/show"})
	a.Equal(acs.GetPermissionAllowedActions("foo"), []string{"foo/index", "foo/show"})
	a.Equal(acs.GetPermissionAllowedActions("foo", "bar"), []string{"foo/index", "foo/show", "bar/show"})
}
